﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;

namespace VMSUI
{
    class CVCamera : ICamera
    {
        public WriteableBitmap Buffer { get; set; }
        public Mat CVBuffer { get; set; }
        public Image Parent { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public string Name { get; set; }
        public double Fps { get; set; }

        VideoCapture _capture = null;
        Mat _frame = null;
        public CVCamera(Image parent)
        {
            this.Parent = parent;
        }
        public void Dispose()
        {
            
        }
        
        public void Load(string Uri)
        {
            try
            {
                _capture = new VideoCapture();
                _frame = new Mat();
                _capture.ImageGrabbed += _capture_ImageGrabbed;
                

            }catch(Exception ex)
            {
                
            }
        }

        private void _capture_ImageGrabbed(object sender, EventArgs e)
        {
            if (Parent != null)
                Parent.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new LiveNotificationDelegate(DoLiveNotification), e);
        }
        delegate void LiveNotificationDelegate(EventArgs e);
        private void DoLiveNotification(EventArgs e)
        {
            if (_capture != null && _capture.Ptr != IntPtr.Zero)
            {
                _capture.Retrieve(_frame);
                CvInvoke.Rectangle(_frame, new System.Drawing.Rectangle(10, 10, 100, 100), new Bgr(0, 0, 255).MCvScalar,3);
                Parent.Source = BitmapSourceConvert.ToBitmapSource(_frame);
                if (Buffer != null)
                {
                    Buffer.Lock();

                    
                    Buffer.Unlock();
                    Console.WriteLine("frame draw"); 
                }
            }
        }
        public void Pause()
        {
            
        }

        public void Play()
        {
            if(_capture!=null)
                _capture.Start();   
        }

        public void UpdateWidthHeight(double width, double height)
        {
            
        }
    }
}
