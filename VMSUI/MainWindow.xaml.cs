﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMSkin.WPF;

namespace VMSUI
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow 
    {

        List<Image> imageviews = null;

        public MainWindow()
        {
            InitializeComponent();

            jmonlogo.Source = new BitmapImage(new Uri("JMON_PNG.png", UriKind.Relative));

<<<<<<< HEAD
            sdk.LoginAsCurrentUser("http://localhost");
            items = sdk.GetPhysicalCameraList();
            Console.WriteLine(items.Count + "Cameras Detected");
            AddCameraToTreeView();
            CameraListView.UpdateLayout();
=======
          
>>>>>>> 247147f51b70b69d8d178e443741b369be14cf31

        }

        private void AddCameraToTreeView()
        {
<<<<<<< HEAD
            if (items == null || items.Count < 1) return;
            TreeViewItem root = new TreeViewItem();
            root.Header = "카메라 목록";
            foreach(Item c in items)
            {
                TreeViewItem item = new TreeViewItem();
                item.Background = new SolidColorBrush(Color.FromRgb())
                item.Header = c.Name;
                root.Items.Add(item);
            }
            CameraListView.Items.Add(root);
=======
          
>>>>>>> 247147f51b70b69d8d178e443741b369be14cf31
        }
        private void SetCameraGridCol(int value)
        {
            int current = MainGrid.ColumnDefinitions.Count;
            if (value < 0) return;
            if (value < current)
            {
                for (int i = 0; i < current - value; i++)
                {
                    MainGrid.ColumnDefinitions.RemoveAt(MainGrid.ColumnDefinitions.Count - 1);
                }
            }
            else
            {
                for (int i = 0; i < value - current; i++)
                {
                    ColumnDefinition r = new ColumnDefinition();
                    r.Width = new GridLength(1, GridUnitType.Star);
                    MainGrid.ColumnDefinitions.Add(r);
                }
            }
        }

        private void SetCameraGridRow(int value)
        {
            int current = MainGrid.RowDefinitions.Count;
            if (value < 0) return;
            if (value < current)
            {
                for (int i = 0; i < current - value; i++)
                {
                    MainGrid.RowDefinitions.RemoveAt(MainGrid.RowDefinitions.Count - 1);
                }
            }
            else
            {
                for (int i = 0; i < value - current; i++)
                {
                    RowDefinition r = new RowDefinition();
                    r.Height = new GridLength(1, GridUnitType.Star);
                    MainGrid.RowDefinitions.Add(r);
                }
            }
        }

        private void SetGrid1(object sender, RoutedEventArgs e)
        {

        }

        private Image CreateImageView(int row, int col, int tag)
        {
            Image i = new Image();
            i.BeginInit();
            i.HorizontalAlignment = HorizontalAlignment.Stretch;
            i.VerticalAlignment = VerticalAlignment.Stretch;
            i.Width = MainGrid.ColumnDefinitions[0].ActualWidth;
            i.Height = MainGrid.RowDefinitions[0].ActualHeight;
            i.Stretch = Stretch.Uniform;
            
            i.SizeChanged += I_SizeChanged;
            i.Tag = tag;
            i.Name = "Image_" + tag.ToString();
            //Console.WriteLine("Actual Width={0}", i.ActualWidth.ToString());
            MainGrid.Children.Add(i);
            Grid.SetRow(i, row);
            Grid.SetColumn(i, col);

            i.EndInit();
            return i;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int cnt = 0;
            int totalRow = MainGrid.RowDefinitions.Count();
            int totalCol = MainGrid.ColumnDefinitions.Count();

            if (imageviews == null) imageviews = new List<Image>();
            //if (cameras == null) cameras = new List<MIPCamera>();
            Image c1 = CreateImageView(0, 0, 0);
            CVCamera cvcam = new CVCamera(c1);
            cvcam.Load("");
            cvcam.Play();
         

        }

        private void I_SizeChanged(object sender, SizeChangedEventArgs e)
        {/*
            Image i = sender as Image;
            Console.WriteLine(i.Tag);
            string[] tok = i.Name.Split('_');
            Console.WriteLine(int.Parse(tok[1]));*/
            Console.WriteLine("I_SizeChanged");
        }

        private void MainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
<<<<<<< HEAD
            if (cameras == null) return;
            double height = MainGrid.RowDefinitions[0].ActualHeight;
            double width = MainGrid.ColumnDefinitions[0].ActualWidth;
            foreach (MIPCamera c in cameras)
            {
                c.UpdateWidthHeight(width, height);
            }
=======
          
>>>>>>> 247147f51b70b69d8d178e443741b369be14cf31

            Console.WriteLine("MainGrid_SizeChanged");
        }

        private void OnGridResized(object sender, SizeChangedEventArgs e)
        {

            Console.WriteLine("OnGridResized");

        }

        private void AddSingleView()
        {
         
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AddSingleView();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
