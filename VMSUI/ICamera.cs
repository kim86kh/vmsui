﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Emgu.CV;
namespace VMSUI
{
    interface ICamera:IDisposable
    {
        WriteableBitmap Buffer { get; set; }
        Mat CVBuffer { get; set; }
        System.Windows.Controls.Image Parent { get; set; }

        double Width { get; set; }
        double Height { get; set; }
        string Name { get; set; }
        double Fps { get; set; }

        void Load(string Uri);
        void Play();
        void Pause();
        void UpdateWidthHeight(double width, double height);


    }
}
