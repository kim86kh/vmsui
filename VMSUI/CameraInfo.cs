﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VMSUI
{
    class CameraInfo
    {
        public string URI { get; set; } = null;
        public string Name { get; set; } =null;
        
        public CameraInfo(string uri, string name)
        {
            this.URI = uri;
            this.Name = name;
        }
    }
}
